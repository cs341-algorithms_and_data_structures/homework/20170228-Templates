//============================================================================
// Name        : Jose Madureira
// Description : Binary tree using templates
//============================================================================

#include <iostream>
using namespace std;

template <typename E > class Iterator;
template <typename E > class NodeList;
template <typename E >
class Node {
	private:
		E elem;
		Node <E>*prev;
		Node <E>*next;
		friend class Iterator <E> ;
		friend class NodeList <E> ;
};

template <typename E > class NodeList;
template <typename E >
class Iterator {
	public:
		E& operator *() {  //returns node value
			return v->elem;
		}
		
		bool operator ==(const Iterator &p) const {
			return v == p.v;
		}
		
		bool operator !=(const Iterator &p) const {
			return v != p.v;
		}
		
		Iterator& operator ++() {
			v= v->next;
			return *this;
		}
		
		Iterator& operator --() {
			v= v->prev;
			return *this;
		}
		
		friend class NodeList <E> ;

	private:
		Node <E>*v;

		Iterator(Node <E> *u) {
			v= u;
		}  //private constructor		
		
};

template <typename E >
class NodeList {
	public:
		NodeList() {
			n= 0;
			header= new Node <E>;
			trailer= new Node <E>;
			header->next= trailer;  //link head with tail
			trailer->prev= header;  //link tail with head
		}
		
		int size() const {
			return n;
		}
		
		bool empty() const {
			return n == 0;
		}
		
		Iterator <E> begin() const {
			return Iterator <E>(header->next);
		}
		
		Iterator <E> end() const {
			return Iterator <E>(trailer);
		}
		
		void insert(const Iterator <E> &p, const E& e) {  //re-links nodes
			Node <E>*w= p.v;
			Node <E>*u= w->prev;
			Node <E>*v= new Node <E>;
			v->elem= e;
			v->next= w;
			w->prev= v;
			v->prev= u;
			u->next= v;
			++n;
		}
		
		void insertFront(const E& e) {
			insert(begin(), e);
		}
		
		void insertBack(const E& e) {
			insert(end(), e);
		}
		
		void erase(const Iterator <E> &p) {
			Node <E>*v= p.v;
			Node <E>*w= v->next;
			Node <E>*u= v->prev;
			u->next= w;
			w->prev= u;
			delete v;
			--n;
		}
		
		void eraseFront() {
			erase(begin());
		}
		
		void eraseBack() {
			erase(--end());
		}
		
		bool print() {
			if (empty()) {
				cout << "\n\t/!\\ Empty List /!\\\n";
				return 1;
			} else {
				Iterator <E> traverse= begin();
				
				int ctr= 0;
				while (ctr < n) {
					cout << "\n" << *traverse;
					++traverse;
					++ctr;
				}
				
				cout << endl;
			}
			return 0;
		}
		
	private:
		int n;
		Node <E>* header;
		Node <E>* trailer;
}
;

int main() {
	NodeList <int> *nl= new NodeList <int>;
	
	bool quit= false;
	while (!quit) {
		cout << "\n  Main Menu "
			 "\n\t(1) Insert Value"
			 "\n\t(2) View List Size"
			 "\n\t(3) Erase Values"
			 "\n\t(4) Print Values"
			 "\n\t(q) Quit Program\n"
			 ;
		
		int value= 0;
		string choice= "0", subChoice= "0";
		
		cout << "\n>>enter choice: ";
		getline(cin, choice);
		
		switch (choice.at(0)) {
			case '1':
				cout << "\n\tInsert at"
					 "\n\t\t(1) Front"
					 "\n\t\t(2) Back\n\n";
				
				do {  //validate user input
					cout << ">>enter choice: ";
					getline(cin, subChoice);
				} while ((subChoice.at(0) < '1' || subChoice.at(0) > '2'));
				
				cout << ">>enter value: ";
				cin >> value;
				cin.get();  //eat newline
				
				if (subChoice.at(0) == '1') nl->insertFront(value);
				else nl->insertBack(value);
				break;
				
			case '2':
				if (nl->empty()) cout << "\n\t\t # The list is empty\n";
				else cout << "\n\t\t # List size: " << nl->size() << endl;
				break;
				
			case '3':
				if (nl->empty()) {
					cout << "\n\t\t # The list is empty\n";
					break;
				}
				
				//TODO erase front and back
				cout << "\n\tErase at"
					 "\n\t\t(1) Front"
					 "\n\t\t(2) Back\n\n";
				
				do {  //validate user input
					cout << ">>enter choice: ";
					getline(cin, subChoice);
				} while ((subChoice.at(0) < '1' || subChoice.at(0) > '2'));
				
				if (subChoice.at(0) == '1') nl->eraseFront();
				else nl->eraseBack();
				break;
				
			case '4':
				nl->print();
				break;
				
			case 'q':
				quit= true;
				break;
				
			default:  //input error handler
				do {
					cout << ">>choice: ";
					getline(cin, choice);
					
					if (choice.at(0) == 'q') {  //force quit program
						quit= true;
						break;
					}
				}
				while ((choice.at(0) < '1' || choice.at(0) > '3'));
				
				break;
				
		}
		
	}
	
	delete nl;
	
	cout << "\n\t\tGoodbye!\n\n";
	return 0;
}
